SRC= $(wildcard *.c)
OBJDIR=.obj
OBJ= $(patsubst %.c,$(OBJDIR)/%.o, $(SRC))
INSTALL_LOCATION=$(DESTDIR)/usr/bin
EXEC=myhello
CFLAGS += -Wall -Wextra -Wno-missing-braces
LDLIBS=

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS) $(LDLIBS)
$(OBJDIR)/%.o: %.c
	$(CC) -c $< -o $@ $(CFLAGS)
install: 
	mkdir -p $(INSTALL_LOCATION)
	cp $(EXEC) $(INSTALL_LOCATION)
	chmod 755 $(INSTALL_LOCATION)/$(EXEC)
clean:
	$(RM) -f $(EXEC) $(OBJ)
