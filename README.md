# myhello-2.10 package

Just a simple myhello package for pbuilder


# Installation 

Generate pbuilder environment (debian buster amd64) :

```
sudo pbuilder create --basetgz $HOME/base-buster.tgz --distribution buster --architecture amd64
```

Build : 

```
cd debian-package-pbuilder/myhello-2.10
pdebuild -- --basetgz $HOME/base-buster.tgz 
```

Generate .deb package : 

```
cd debian-package-pbuilder
sudo pbuilder --build --basetgz ~/base-buster.tgz myhello_2.10-1.dsc 
```

The result is in `/var/cache/pbuilder/result`


We can easily cross build our package by modifying `--architecture` option (i.e. `--architecture arm64` ). 
